package server;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

import javax.net.ServerSocketFactory;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Hangfan Li
 */
public class Server {
    // Declare the port number
    private static int port = 3000;

    // Identifies the user number connected
    private static int counter = 0;

    public static final String success = "success";
    public static final String fail = "fail";

    public static void main(String[] args) {

        if (args.length > 0) {
            if (args[0] != null) {
                port = Integer.parseInt(args[0]);
            }else{
                port = 3000;
            }

        }

        ServerSocketFactory factory = ServerSocketFactory.getDefault();
        try (ServerSocket server = factory.createServerSocket(port)) {

            System.out.println("Server started");
            System.out.println("Waiting for client connection..");

            // Wait for connections.
            while (true) {
                Socket client = server.accept();
                counter++;
                System.out.println("Client " + counter + ": Applying for SERVICE!");

                //thread-per-request architecture: create a new thread for each accepted request
                Thread t = new Thread(() -> serveClient(client));
                t.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Construct command line options
     *
     * @return CommandLine options.
     */
    private static Options cmdLineOptions() {
        // Build up command line options
        Options options = new Options();

        options.addOption("hello", false, "say hello to name input");

        // parse command line arguments
        return options;
    }

    private static String handleRequest(String request, Socket client) throws IOException {

        String[] args = request.split(",");
        Options options = cmdLineOptions();

        System.out.println("requestType: " + args[0]);

        String requestType = args[0].toLowerCase();

        switch (requestType) {
            case "hello": {

                if (args[1] != null){
                    String result = "hello " + args[1];
                    return result;
                }else{
                    return fail;
                }

            }

            default: {
                HelpFormatter helpFormatter = new HelpFormatter();
                helpFormatter.printHelp("Server", options);
                return "Command is invalid";
            }
        }
    }

    private static void serveClient(Socket client) {
        try (Socket clientSocket = client) {
            // Input stream
            DataInputStream input = new DataInputStream(clientSocket.getInputStream());
            // Output Stream
            DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());

            String request = input.readUTF();

            System.out.println("CLIENT " + counter + " : " + request);

            System.out.println("Server: Hi Client " + counter + " !");

            output.writeUTF(handleRequest(request, client));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
