package client;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Hangfan Li
 */
public class ClientGUI {
    private JPanel Client;
    private JTextField textField1;
    private JButton submitButton;

    public static void main(String[] args) {
        JFrame frame = new JFrame("ClientGUI");
        frame.setContentPane(new ClientGUI().Client);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public ClientGUI() {
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = textField1.getText().trim();
                if (name.length() > 0) {
                    String result = ClientConnection.helloWorld(name);
                    if (result != null) {
                        textField1.setText(result);
                    } else {
                        JOptionPane.showMessageDialog(null, "Failed", "Error",
                                JOptionPane.WARNING_MESSAGE);

                    }
                } else {
                    JOptionPane.showMessageDialog(null,
                            "The name cannot be empty! Please input the name to search in the 'notification row'.", "Error",
                            JOptionPane.WARNING_MESSAGE);
                }

            }
        });
    }
}
