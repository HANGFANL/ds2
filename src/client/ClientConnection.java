package client;

import javax.swing.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @author Hangfan Li
 */
public class ClientConnection {
    // server-address
    public static String serverAddress = "localhost";
    // server-port
    public static int serverPort = 3000;

    private static String requestConnection(String command) {

        try (Socket socket = new Socket(serverAddress, serverPort);) {

            // Output and Input Stream
            DataInputStream input = new DataInputStream(socket.getInputStream());
            DataOutputStream output = new DataOutputStream(socket.getOutputStream());

            output.writeUTF(command);
            output.flush();

            String serverMessage = input.readUTF();
            return serverMessage;

        } catch (ConnectException e) {

            JOptionPane.showMessageDialog(null, "Ｃannot connect server", "Error", JOptionPane.ERROR_MESSAGE);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (EOFException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch(IOException e) {
            // handle exception which is not expected
            e.printStackTrace();
        }
        return null;
    }

    public static void setServerAddress(String serverAddress) {
        // TODO Auto-generated method stub
        ClientConnection.serverAddress = serverAddress;

    }

    public static String getServerAddress() {
        // TODO Auto-generated method stub
        return serverAddress;
    }

    public static void setServerPort(int serverPort) {
        // TODO Auto-generated method stub
        ClientConnection.serverPort = serverPort;

    }

    public static int getServerPort() {
        // TODO Auto-generated method stub
        return serverPort;
    }

    public static String helloWorld(String name) {
        // TODO Auto-generated method stub
        return requestConnection("hello" + "," + name);
    }

}
